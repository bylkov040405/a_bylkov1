chisla = list(range(1,10))
def play_board(chisla):
    print('-'*13)
    for i in range(3):
        print('|', chisla[0+i*3],'|',chisla[1+i*3],'|',chisla[2+i*3],'|')
        print('-'*13)
play_board(chisla)
def take_input(play_taken):
    valid = False
    while not valid:
        player_answer = input('Куда поставите ' +play_taken+ '?\n>')
        try:
            player_answer = int(player_answer)
        except:
            print('числа введены неверно!')
            continue
        if player_answer>=1 and player_answer<=9:
            if (str(chisla[player_answer - 1])) not in 'xo':
                chisla[player_answer - 1] = play_taken
                valid = True
            else:
                print('Клетка занята')
        else:
            print('неверный ввод; введенные числа должны быть от 1 до 9')
def check_winner(chisla):
    winner_coord = ((0,3,6),(1,4,7),(2,5,8),(0,1,2),(3,4,5),(6,7,8),(0,4,8),(2,4,6))
    for i in winner_coord:
        if chisla[i[0]] == chisla[i[1]] == chisla[i[2]]:
            return chisla[i[0]]
    return False
def main(chisla):
    counter = 0
    winner = False
    while not winner:
        play_board(chisla)
        if counter%2 == 0:
            take_input('x')
        else: take_input('0')
        counter += 1
        if counter>4:
            tmp = check_winner(chisla)
            if tmp:
                print(tmp, 'одержал попеду!')
                winner = True
                break
        if counter == 9:
            print('ничья')
            break
    play_board(chisla)
main(chisla)
            



    
        
